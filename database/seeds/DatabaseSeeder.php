<?php

use Illuminate\Database\Seeder;
use App\Model\Client;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $data = [];

        for($i = 1; $i <= 200 ; $i++) {
            array_push($data, [
                'name' => $faker->name,
                'email' => $faker->unique()->email,
                'description' => $faker->text,
                'created_at' => \Carbon\Carbon::now(),
            ]);
        }

        Client::insert($data);
    }
}
