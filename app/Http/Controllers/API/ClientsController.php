<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\StoreUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Client;
use Illuminate\Support\Facades\Validator;

class ClientsController extends Controller
{

    /**
     * @return mixed
     */
    public function index()
    {
        return Client::paginate(10);
    }

    /**
     * @param Client $client
     * @return Client
     */
    public function show(Client $client)
    {
        return $client;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:clients',
        ]);

        if ($validator->fails())
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()], 404);

        $client = Client::create($request->all());

        return response()->json($client);
    }

    /**
     * @param Request $request
     * @param Client $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Client $client)
    {
        $client->update($request->all());

        return response()->json($client);
    }

    /**
     * @param Client $client
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Client $client)
    {
        $client->delete();

        return response()->json(['status' => 'success', 'message' => 'Cliente ' . $client->id . ' removido.' ]);
    }
}
