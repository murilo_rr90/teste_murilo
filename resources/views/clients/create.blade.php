@extends('layouts.app')
@section('model', 'Clientes')
@section('action', 'Criar')

@section('content')
    {{ Form::open(array('url' => 'clients/store', 'method' => 'POST')) }}

        <div class="form-group">
            {{ Form::label('name', 'Nome') }}
            {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
        </div>

        <div class="form-group">
            {{ Form::label('email', 'Email') }}
            {{ Form::email('email', Input::old('email'), array('class' => 'form-control')) }}
        </div>

        <div class="form-group">
            {{ Form::label('description', 'Descrição') }}
            {{ Form::textarea('description', null, array('class' => 'form-control')) }}
        </div>

        {{ Form::submit('Salvar', array('class' => 'btn btn-success')) }}
    {{ Form::close() }}
@endsection