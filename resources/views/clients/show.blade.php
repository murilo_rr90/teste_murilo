@extends('layouts.app')
@section('model', 'Clientes')
@section('action', 'Exibir')


@section('content')
    <div class="jumbotron text-center">
        <h2>{{ $client->name }}</h2>
        <p>
            <strong>Email:</strong> {{ $client->email }}<br>
            <strong>Criado:</strong> {{ !is_null($client->created_at) ? $client->created_at->format('d/m/Y H:i:s') :  '' }}<br>
            <strong>Descrição:</strong> {{ $client->description }}
        </p>
    </div>
@endsection