@extends('layouts.app')
@section('model', 'Clientes')
@section('action', 'Editar')

@section('content')
    {{ Html::ul($errors->all()) }}

    {{ Form::model($client, array('route' => array('clients.update', $client->id), 'method' => 'PUT')) }}
        <div class="form-group">
            {{ Form::label('name', 'Nome') }}
            {{ Form::text('name', null, array('class' => 'form-control')) }}
        </div>

        <div class="form-group">
            {{ Form::label('email', 'Email') }}
            {{ Form::email('email', null, array('class' => 'form-control')) }}
        </div>

        <div class="form-group">
            {{ Form::label('description', 'Descrição') }}
            {{ Form::textarea('description', null, array('class' => 'form-control')) }}
        </div>
        {{ Form::submit('Salvar', array('class' => 'btn btn-success')) }}
    {{ Form::close() }}
@endsection