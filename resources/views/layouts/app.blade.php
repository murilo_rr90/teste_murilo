<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">

        <main class="py-4">
            <div class="container">
                <nav class="navbar navbar-inverse">
                    <div class="navbar-header">
                        <h1>@yield('model')<span style="font-size: 15px"> @yield('action')</span></h1>
                    </div>
                    <ul class="nav navbar-nav">
                        <li><a href="{{ route('clients.index') }}">Listar todos</a></li>
                        <li><a href="{{ route('clients.create') }}">Criar Novo</a></li>
                    </ul>
                </nav>
                @include('layouts.flash')
                @yield('content')
            </div>
        </main>
    </div>
</body>
</html>
